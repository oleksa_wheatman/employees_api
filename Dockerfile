FROM ruby:3.0.1
RUN apt-get update -qq && apt-get install build-essential libpq-dev -y
RUN mkdir /app
WORKDIR /app
COPY . .

