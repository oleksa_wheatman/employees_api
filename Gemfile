# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.0.1"

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails', branch: 'main'
gem "rails", "~> 6.1.3", ">= 6.1.3.1"
# Use postgresql as the database for Active Record
# Use Puma as the app server
gem "puma", "~> 5.0"

gem "pg", "~> 1.1"
gem "devise"
gem "jwt"
gem "rack-cors"
gem "rswag"
gem "rspec-rails", "~> 5.0.0"
gem "concurrent-ruby", "~> 1.1.8", require: "concurrent"
gem "irb", require: false

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem "jbuilder", "~> 2.7"
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem "bootsnap", ">= 1.4.4", require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
# gem 'rack-cors'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem "byebug", platforms: [:mri, :mingw, :x64_mingw]
  gem "ffaker"
  gem "factory_bot_rails"
  gem "rubocop"
  gem "rubocop-packaging"
  gem "rubocop-rspec"
  gem "rubocop-performance"
  gem "rubocop-rails", require: false
  gem "json-schema_builder"
end

group :development do
  gem "listen", "~> 3.3"
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem "spring"
  gem "brakeman"
  gem "database_consistency", group: :development, require: false
  gem "lefthook"
end

group :test do
  gem "pry", "~> 0.13.1"
  gem "simplecov", require: false
  gem "shoulda-matchers", "~> 4.0"
  gem "n_plus_one_control"
  gem "bundler-audit"
  gem "bundler-leak"
end


# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: [:mingw, :mswin, :x64_mingw, :jruby]
