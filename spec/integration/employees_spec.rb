# frozen_string_literal: true

require "swagger_helper"

describe "Employees API" do
  User.create(email: "fake@mail7.com", password: "password", username: "fakename7")
  token = User.last.generate_jwt
  let!(:Authorization) { "Token #{token}" }
  let(:params) { { first_name: "fake", last_name: "fake", salary: 500 } }
  let(:employee) { Employee.create(first_name: "fake", last_name: "morefake", salary: 500) }
  let(:id) { employee.id }

  path "/api/employees" do
    post "Creates an employee" do
      tags "Employees"
      consumes "application/json"
      produces "application/json"
      security [ApiKeyAuth: {}]

      expected_request_schema = SpecSchemas::EmployeeCreateRequest.new
      parameter name: :params, in: :body, schema: expected_request_schema.schema.as_json

      response "200", "OK" do
        expected_response_schema = SpecSchemas::EmployeeResponse.new
        schema(expected_response_schema.schema.as_json)

        before do |example|
          submit_request(example.metadata)
        end

        it_behaves_like "a JSON endpoint", 200 do
          let(:expected_response_schema) { expected_response_schema }
          let(:expected_request_schema) { expected_request_schema }
        end
      end

      response "422", "Unprocessable Entity" do
        let(:params) {
          { first_name: "fake" }
        }
        run_test!
      end

      response "401", "Unauthorized" do
        let(:Authorization) { "Token " }
        run_test!
      end
    end
  end

  path "/api/employees/{id}" do
    put "Update employee details" do
      tags "Employees"
      consumes "application/json"
      produces "application/json"
      security [ApiKeyAuth: {}]
      parameter name: :id, in: :path, type: :string

      expected_request_schema = SpecSchemas::EmployeeCreateRequest.new
      parameter name: :params, in: :body, schema: expected_request_schema.schema.as_json

      response "200", "OK" do
        expected_response_schema = SpecSchemas::EmployeeResponse.new
        schema(expected_response_schema.schema.as_json)

        before do |example|
          submit_request(example.metadata)
        end

        it_behaves_like "a JSON endpoint", 200 do
          let(:expected_response_schema) { expected_response_schema }
          let(:expected_request_schema) { expected_request_schema }
        end
      end

      response "404", "Error: Not Found" do
        let(:id) { 0 }
        run_test!
      end

      response "401", "Unauthorized" do
        let(:Authorization) { "Token " }
        run_test!
      end
    end
  end

  path "/api/employees" do
    get "Show all employees" do
      tags "Employees"
      produces "application/json"
      security [ApiKeyAuth: {}]

      response "200", "OK" do
        schema type: :object,
               properties: {
                 first_name: { type: :string },
                 last_name: { type: :string },
                 salary: { type: :integer }
               }
        run_test!
      end

      response "401", "Unauthorized" do
        let(:Authorization) { "Token " }
        run_test!
      end
    end
  end

  path "/api/employees/{id}" do
    get "Show employee details" do
      tags "Employees"
      produces "application/json"
      security [ApiKeyAuth: {}]
      parameter name: :id, in: :path, type: :string

      response "200", "OK" do
        expected_response_schema = SpecSchemas::EmployeeResponse.new
        schema(expected_response_schema.schema.as_json)

        before do |example|
          submit_request(example.metadata)
        end

        it "matches the documented response schema" do |example|
          json_response = JSON.parse(response.body)
          errors = expected_response_schema.schema.fully_validate(json_response)
          expect(errors).to be_empty
        end

        it "returns a valid 200 response" do |example|
          expect(response.status).to eq(200)
        end
        run_test!
      end

      response "404", "Error: Not Found" do
        let(:id) { 0 }
        run_test!
      end

      response "401", "Unauthorized" do
        let(:Authorization) { "Token " }
        run_test!
      end
    end
  end

  path "/api/employees/{id}" do
    delete "Deletes employee details" do
      tags "Employees"
      security [ApiKeyAuth: {}]
      parameter name: :id, in: :path, type: :string

      response "204", "No Content" do
        run_test!
      end

      response "404", "Error: Not Found" do
        let(:id) { 0 }
        run_test!
      end

      response "401", "Unauthorized" do
        let(:Authorization) { "Token " }
        run_test!
      end
    end
  end
end
