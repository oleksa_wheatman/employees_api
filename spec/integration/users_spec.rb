# frozen_string_literal: true

require "swagger_helper"

describe "Users API" do
  let(:params) {
    { user: { email: "new@mail.com", password: "password", username: "newuser" } }
  }
  User.create(email: "thebest@mail.com", password: "password", username: "absolutly")
  token = User.last.generate_jwt
  let!(:Authorization) { "Token #{token}" }

  path "/api/users" do
    post "Creates a user" do
      tags "Users"
      consumes "application/json"
      produces "application/json"
      expected_request_schema = SpecSchemas::UserCreateRequest.new
      parameter name: :params, in: :body, schema: expected_request_schema.schema.as_json


      response "200", "OK" do
        expected_response_schema = SpecSchemas::UserResponse.new
        schema(expected_response_schema.schema.as_json)


        before do |example|
          submit_request(example.metadata)
        end

        it "matches the documented response schema" do |example|
          json_response = JSON.parse(response.body)
          errors = expected_response_schema.schema.fully_validate(json_response)
          expect(errors).to be_empty
        end

        it "matches the documented request schema" do |example|
          errors = expected_request_schema.schema.fully_validate(params)
          puts params if errors.count > 0 # for debugging
          expect(errors).to be_empty
        end

        it "returns a valid 200 response" do |example|
          expect(response.status).to eq(200)
        end
      end

      response "422", "Error: Unprocessable Entity" do
        let(:params) {
          { user: { email: "new@mail.com", password: "password" } }
        }
        run_test!
      end
    end
  end

  path "/api/users/login" do
    post "Login user" do
      tags "Users"
      consumes "application/json"
      produces "application/json"
      expected_request_schema = SpecSchemas::UserCreateRequest.new
      parameter name: :params, in: :body, schema: expected_request_schema.schema.as_json

      let(:params) {
        { user: { email: "thebest@mail.com", password: "password", username: "absolutly" } }
      }
      response "200", "OK" do
        expected_response_schema = SpecSchemas::UserResponse.new
        schema(expected_response_schema.schema.as_json)


        before do |example|
          submit_request(example.metadata)
        end

        it "matches the documented response schema" do |example|
          json_response = JSON.parse(response.body)
          errors = expected_response_schema.schema.fully_validate(json_response)
          expect(errors).to be_empty
        end

        it "matches the documented request schema" do |example|
          errors = expected_request_schema.schema.fully_validate(params)
          puts params if errors.count > 0 # for debugging
          expect(errors).to be_empty
        end

        it "returns a valid 200 response" do |example|
          expect(response.status).to eq(200)
        end
        run_test!
      end

      response "422", "Error: Unprocessable Entity" do
        let(:params) {
          { user: { email: "new@mail.com", password: "password" } }
        }
        run_test!
      end
    end
  end

  path "/api/user" do
    get "Shows user information" do
      tags "Users"
      produces "application/json"
      security [ApiKeyAuth: {}]

      token = User.last.generate_jwt
      let!(:Authorization) { "Token #{token}" }

      response "200", "OK" do
        expected_response_schema = SpecSchemas::UserResponse.new
        schema(expected_response_schema.schema.as_json)


        before do |example|
          submit_request(example.metadata)
        end

        it "matches the documented response schema" do |example|
          json_response = JSON.parse(response.body)
          errors = expected_response_schema.schema.fully_validate(json_response)
          expect(errors).to be_empty
        end
      end

      response "401", "Unauthorized" do
        let(:Authorization) { "Token " }
        run_test!
      end
    end
  end

  path "/api/user" do
    put "Updates user information" do
      tags "Users"
      consumes "application/json"
      produces "application/json"
      security [ApiKeyAuth: {}]
      expected_request_schema = SpecSchemas::UserCreateRequest.new
      parameter name: :params, in: :body, schema: expected_request_schema.schema.as_json


      response "200", "OK" do
        expected_response_schema = SpecSchemas::UserResponse.new
        schema(expected_response_schema.schema.as_json)


        before do |example|
          submit_request(example.metadata)
        end

        it "matches the documented response schema" do |example|
          json_response = JSON.parse(response.body)
          errors = expected_response_schema.schema.fully_validate(json_response)
          expect(errors).to be_empty
        end

        it "matches the documented request schema" do |example|
          errors = expected_request_schema.schema.fully_validate(params)
          puts params if errors.count > 0 # for debugging
          expect(errors).to be_empty
        end

        it "returns a valid 200 response" do |example|
          expect(response.status).to eq(200)
        end
      end

      response "422", "Error: Unprocessable Entity" do
        let(:params) {
          { user: { email: "" } }
        }
        run_test!
      end

      response "401", "Unauthorized" do
        let(:Authorization) { "Token " }
        run_test!
      end
    end
  end
end
