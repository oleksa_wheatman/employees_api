# frozen_string_literal: true

module SpecSchemas
  class UserCreateRequest
    include JSON::SchemaBuilder
    def schema
      object do
        object :user do
          string :email, required: true
          string :password, required: true
          string :username, required: true
        end
      end
    end
  end

  class UserResponse
    include JSON::SchemaBuilder
    def schema
      object do
        object :user do
          number :id, required: true
          string :email, required: true
          string :username, required: true
          string :token, required: true
        end
      end
    end
  end
end
