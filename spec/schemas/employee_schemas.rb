# frozen_string_literal: true

module SpecSchemas
  class EmployeeCreateRequest
    include JSON::SchemaBuilder
    def schema
      object do
        string :first_name, required: true
        string :last_name, required: true
        number :salary, required: true
      end
    end
  end

  class EmployeeResponse
    include JSON::SchemaBuilder
    def schema
      object do
        object :employee do
          number :id, required: true
          string :first_name, required: true
          string :last_name, required: true
          number :salary, required: true
        end
      end
    end
  end
end
