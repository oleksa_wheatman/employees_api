---
openapi: 3.0.1
info:
  title: API V1
  version: v1
paths:
  "/api/employees":
    post:
      summary: Creates an employee
      tags:
      - Employees
      security:
      - ApiKeyAuth: {}
      parameters: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  employee:
                    type: object
                    required:
                    - id
                    - first_name
                    - last_name
                    - salary
                    properties:
                      id:
                        type: number
                      first_name:
                        type: string
                      last_name:
                        type: string
                      salary:
                        type: number
        '422':
          description: Unprocessable Entity
        '401':
          description: Unauthorized
      requestBody:
        content:
          application/json:
            schema:
              type: object
              required:
              - first_name
              - last_name
              - salary
              properties:
                first_name:
                  type: string
                last_name:
                  type: string
                salary:
                  type: number
    get:
      summary: Show all employees
      tags:
      - Employees
      security:
      - ApiKeyAuth: {}
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  first_name:
                    type: string
                  last_name:
                    type: string
                  salary:
                    type: integer
        '401':
          description: Unauthorized
  "/api/employees/{id}":
    put:
      summary: Update employee details
      tags:
      - Employees
      security:
      - ApiKeyAuth: {}
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  employee:
                    type: object
                    required:
                    - id
                    - first_name
                    - last_name
                    - salary
                    properties:
                      id:
                        type: number
                      first_name:
                        type: string
                      last_name:
                        type: string
                      salary:
                        type: number
        '404':
          description: 'Error: Not Found'
        '401':
          description: Unauthorized
      requestBody:
        content:
          application/json:
            schema:
              type: object
              required:
              - first_name
              - last_name
              - salary
              properties:
                first_name:
                  type: string
                last_name:
                  type: string
                salary:
                  type: number
    get:
      summary: Show employee details
      tags:
      - Employees
      security:
      - ApiKeyAuth: {}
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  employee:
                    type: object
                    required:
                    - id
                    - first_name
                    - last_name
                    - salary
                    properties:
                      id:
                        type: number
                      first_name:
                        type: string
                      last_name:
                        type: string
                      salary:
                        type: number
        '404':
          description: 'Error: Not Found'
        '401':
          description: Unauthorized
    delete:
      summary: Deletes employee details
      tags:
      - Employees
      security:
      - ApiKeyAuth: {}
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: string
      responses:
        '204':
          description: No Content
        '404':
          description: 'Error: Not Found'
        '401':
          description: Unauthorized
  "/api/users":
    post:
      summary: Creates a user
      tags:
      - Users
      parameters: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  user:
                    type: object
                    required:
                    - id
                    - email
                    - username
                    - token
                    properties:
                      id:
                        type: number
                      email:
                        type: string
                      username:
                        type: string
                      token:
                        type: string
        '422':
          description: 'Error: Unprocessable Entity'
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                user:
                  type: object
                  required:
                  - email
                  - password
                  - username
                  properties:
                    email:
                      type: string
                    password:
                      type: string
                    username:
                      type: string
  "/api/users/login":
    post:
      summary: Login user
      tags:
      - Users
      parameters: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  user:
                    type: object
                    required:
                    - id
                    - email
                    - username
                    - token
                    properties:
                      id:
                        type: number
                      email:
                        type: string
                      username:
                        type: string
                      token:
                        type: string
        '422':
          description: 'Error: Unprocessable Entity'
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                user:
                  type: object
                  required:
                  - email
                  - password
                  - username
                  properties:
                    email:
                      type: string
                    password:
                      type: string
                    username:
                      type: string
  "/api/user":
    get:
      summary: Shows user information
      tags:
      - Users
      security:
      - ApiKeyAuth: {}
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  user:
                    type: object
                    required:
                    - id
                    - email
                    - username
                    - token
                    properties:
                      id:
                        type: number
                      email:
                        type: string
                      username:
                        type: string
                      token:
                        type: string
        '401':
          description: Unauthorized
    put:
      summary: Updates user information
      tags:
      - Users
      security:
      - ApiKeyAuth: {}
      parameters: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  user:
                    type: object
                    required:
                    - id
                    - email
                    - username
                    - token
                    properties:
                      id:
                        type: number
                      email:
                        type: string
                      username:
                        type: string
                      token:
                        type: string
        '422':
          description: 'Error: Unprocessable Entity'
        '401':
          description: Unauthorized
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                user:
                  type: object
                  required:
                  - email
                  - password
                  - username
                  properties:
                    email:
                      type: string
                    password:
                      type: string
                    username:
                      type: string
servers:
- url: http://localhost:3000/
  variables:
    defaultHost:
      default: http://localhost:3000/
- url: https://employees-db.herokuapp.com/
  variables:
    defaultHost:
      default: https://employees-db.herokuapp.com/
components:
  securitySchemes:
    ApiKeyAuth:
      type: apiKey
      in: header
      name: Authorization
      description: Specify the token with 'Token' prefix
