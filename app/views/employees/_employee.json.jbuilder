# frozen_string_literal: true

json.call(employee, :id, :first_name, :last_name, :salary)
