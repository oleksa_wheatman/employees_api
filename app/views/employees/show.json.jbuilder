# frozen_string_literal: true

json.employee do |json|
  json.partial! "employees/employee", employee: @employee
end
