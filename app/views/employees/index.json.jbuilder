# frozen_string_literal: true

json.employees @employees do |employee|
  json.partial! "employees/employee", employee: employee
end
