# frozen_string_literal: true

class Employee < ApplicationRecord
  validates :first_name, :last_name, :salary, presence: true
end
